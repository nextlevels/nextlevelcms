<?php namespace NextLevels\NextLevelCms\Behaviors;

use Backend\Classes\ControllerBehavior;

class AjaxSortController extends ControllerBehavior
{

    /**
     * @var array Configuration values that must exist when applying the primary config file.
     */
    protected $model;

    /**
     * AjaxSortController constructor.
     *
     * @param $model
     */
    public function __construct($controller)
    {
        $controller->addCss('/plugins/nextlevels/nextlevelcms/assets/css/plugin.css');
        $controller->addJs('https://lukasoppermann.github.io/html5sortable/html5sortable.js');
        $controller->addJs('/plugins/nextlevels/nextlevelcms/assets/js/plugin.js');

        parent::__construct($controller);
        $model = $this->controller->formCreateModelObject();
        $model = $this->controller->formExtendModel($model) ?: $model;
        $this->model = $model;
    }

    /**
     * Refresh Boolean
     *
     * @return mixed
     */
    public function onUpdateBool()
    {
        if (post('action') === 'update') {
            $modelClass = $this->model->sortable[0]['class'];
        } else {
            $modelClass = $this->model;
        }
        $model = new $modelClass;

        $page = $model->find(post('id'));

        $page->active = (post('checked') == 'checked') ? 1 : 0;

        $page->save();

        return $this->controller->listRefresh();
    }

    /**
     * @return mixed
     */
    public function onUpdatePosition()
    {
        $isPivot = isset($this->model->sortable[0]['pivot']);

        if ($this->model->sortable[0] == null) {
            return $this->controller->listRefresh();
        }

        if (!$isPivot) {
            $modelClass = $this->model->sortable[0]['class'];
            $model = new $modelClass;
        } else {
            $parent = $this->model->where('id', post('id'))->first();
        }

        $moved = [];
        $position = 0;

        if (($reorderIds = post('checked')) && is_array($reorderIds) && count($reorderIds)) {
            if ($isPivot) {
                foreach ($reorderIds as $id) {
                    if (in_array($id, $moved) || !$record = $parent->pages->where('id', $id)->first()) {
                        continue;
                    }

                    $record->pivot->sort = $position;
                    $record->pivot->save();
                    $moved[] = $id;
                    $position++;
                }
            } else {
                foreach ($reorderIds as $id) {
                    if (in_array($id, $moved) || !$record = $model::find($id)) {
                        continue;
                    }
                    $record->sort_order = $position;
                    $record->save();
                    $moved[] = $id;
                    $position++;
                }
            }
        }

        return $this->controller->listRefresh();
    }
}
