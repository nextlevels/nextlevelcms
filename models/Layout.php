<?php namespace NextLevels\NextLevelCms\Models;

use Illuminate\Support\Facades\Storage;
use Model;

/**
 * Class Layout
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>
 */
class Layout extends Model
{

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_nextlevelcms_layout';

    /**
     * @var array Relations
     */
    public $hasMany = ['pages' => Page::class];

    public $belongsTo = ['page' => [Page::class]];

    /**
     * @return array
     */
    public function getThemeOptions(): array
    {
        $themes = Storage::disk('root')->directories('/themes/');
        $themes = array_map(function ($value) {
            return pathinfo($value, PATHINFO_FILENAME);
        }, $themes);

        return array_combine($themes, $themes);
    }
}
