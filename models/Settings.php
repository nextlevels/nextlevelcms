<?php namespace NextLevels\NextLevelCms\Models;

use Model;
use System\Behaviors\SettingsModel;

/**
 * Class Settings
 *
 * @author Slawa Ditzel <s.d@next-levels.de>, Next Levels GmbH
 */
class Settings extends Model
{
    /**
     * @var string[] implements
     */
    public $implement = [SettingsModel::class];

    /**
     * @var string settings code
     */
    public $settingsCode = 'nextlevels_nextcms_settings';

    /**
     * @var string settings fields
     */
    public $settingsFields = 'fields.yaml';
}
