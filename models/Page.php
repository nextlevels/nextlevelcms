<?php namespace NextLevels\NextLevelCms\Models;

use Exception;
use Model;
use NextLevels\NextLevelCms\Classes\FileParser;
use October\Rain\Database\Builder;
use October\Rain\Database\Traits\SimpleTree;
use October\Rain\Database\Traits\SoftDelete;

/**
 * Class Page
 */
class Page extends Model
{
    use SimpleTree, SoftDelete;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_nextlevelcms_page';
    /**
     * @var string[]
     */
    public $jsonable = ['content_editor'];
    /**
     * @var string[]
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    /**
     * @var array
     */
    public $translatable = [
        'name',
        ['slug', 'index' => true],
        'seo_title',
        'seo_description',
        'seo_keywords'
    ];
    /**
     * @var string[][]
     */
    public $sortable = [['class' => Element::class]];
    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'menus' => [
            Menu::class,
            'table' => 'nextlevels_nextlevelcms_menu_pages',
            'key' => 'p_id',
            'otherKey' => 'm_id',
            'pivot' => ['sort'],
        ]
    ];
    public $hasMany = [
        'children' => [self::class, 'key' => 'parent_id'],
        'elements' => [
            Element::class,
            'order' => 'sort_order asc'
        ]
    ];
    public $belongsTo = [
        'site' => [Site::class],
        'parent' => self::class,
        'data_layout' => [Layout::class, 'key' => 'layout_id']
    ];
    /**
     * @var string[]
     */
    protected $casts = ['active' => 'boolean'];
    /**
     * @var string[]
     */
    protected $dates = ['deleted_at'];

    /**
     * @return array
     */
    public function getLayoutOptions(): array
    {
        $files = FileParser::instance()->getFileList('/layouts/');

        if (!empty($files)) {
            foreach ($files as $file) {
                $vars = FileParser::instance()->getConfigVariables('/layouts/' . $file . '.htm');

                if (!empty($vars)) {
                    $vars = array_change_key_case($vars);
                    $files[$file] = $vars['description'] . ' (' . $file . ')' ?? $file;
                }
            }
        }

        return $files;
    }

    /**
     * Render html
     *
     * @return string
     */
    public function renderHTML($model = null): string
    {
        $html = '';
        $css = '<style>';

        foreach ($this->elements()->get() as $value) {
            $html .= $value->renderHTML($model);
        }

        foreach ($this->elements()->get() as $value) {
            $css .= $value->content_css;
        }

        $css .= '</style>';

        $script = "<script> 
                    const body = document.querySelector('body');
                    const extraBodyClasses = '" . $this->css_classes . "';
      
                    body.classList.add('nextcms-page'); 
                    
                    if (extraBodyClasses) {
                      body.classList.add(extraBodyClasses); 
                    }
                    </script>";

        return $html . $css . $script;
    }

    /**
     * Clear page and elements cache
     */
    public function clearCache(): void
    {
        foreach ($this->elements()->get() as $value) {
            try {
                $value->save();
            } catch (Exception $exception) {
            }
        }
    }

    /**
     * Filter by site
     *
     * @param Builder $builder
     * @param array $sites
     */
    public function scopeFilterSite(Builder $builder, array $sites = []): void
    {
        $builder->whereIn('site_id', $sites);
    }

    /**
     * Get active records
     *
     * @param Builder $builder
     * @param bool $status
     */
    public function scopeIsActive(Builder $builder, bool $status = true): void
    {
        $builder->where('active', $status);
    }
}
