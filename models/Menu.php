<?php namespace NextLevels\NextLevelCms\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Database\Traits\SoftDelete;

/**
 * Class Menu
 */
class Menu extends Model
{
    use Sluggable, SoftDelete;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_nextlevelcms_menu';
    /**
     * @var string[]
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    /**
     * @var array
     */
    public $translatable = [
        'name'
    ];
    /**
     * @var array[]
     */
    public $sortable = [
        [
            'class' => Page::class,
            'pivot' => ['sort'],
        ]
    ];
    /**
     * @var array[] relations
     */
    public $belongsToMany = [
        'pages' => [
            Page::class,
            'table' => 'nextlevels_nextlevelcms_menu_pages',
            'key' => 'm_id',
            'otherKey' => 'p_id',
            'pivot' => ['sort'],
        ]
    ];
    /**
     * @var string[]
     */
    protected $dates = ['deleted_at'];
    /**
     * @var string[]
     */
    protected $slugs = ['code' => 'name'];
}
