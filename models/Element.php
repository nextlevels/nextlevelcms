<?php namespace NextLevels\NextLevelCms\Models;

use Cms\Classes\CmsException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Model;
use NextLevels\NextLevelCms\Classes\FileParser;
use October\Rain\Parse\Syntax\Parser as SyntaxParser;

/**
 * Class Element
 */
class Element extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_nextlevelcms_element';
    /**
     * @var string[]
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    /**
     * @var string
     */
    public $templateElementsPath = '/partials/elements/';
    /**
     * @var string[]
     */
    public $translatable = [
        'content_html',
        'content_editor',
        'editor_save',
        'content_css',
        'content',
        'name',
    ];
    /**
     * @var string[][] relations
     */
    public $belongsTo = ['page' => [Page::class]];
    public $hasMany = ['elements' => self::class];
    /**
     * @var string[]
     */
    protected $fillable = [
        'parent',
        'content_html',
        'content_editor',
        'editor_save',
        'content_css',
        'content',
        'template',
        'editor_save',
        'sort_order',
        'name',
    ];
    /**
     * @var array Guarded fields
     */
    protected $jsonable = ['content', 'content_editor'];

    /**
     * @return array
     */
    public function getTemplateOptions(): array
    {
        //Add theme elements
        return $this->getElements($this->templateElementsPath);
    }

    /**
     * get all elements in the given filepath
     *
     * @return array
     */
    private function getElements($path): array
    {
        $files = FileParser::instance()->getFileList($path);

        if (!empty($files)) {
            foreach ($files as $file) {
                $vars = FileParser::instance()->getConfigVariables($path . $file . '.htm');

                if (!empty($vars)) {
                    $vars = array_change_key_case($vars);
                    $files[$file] = $vars['name'] . ' (' . $file . ')' ?? $file;
                }
            }
        }

        return $files;
    }

    /**
     * Get template name from template config
     *
     * @return string
     */
    public function getTemplateNameAttribute(): string
    {
        $template = $this->template;
        $vars = array_change_key_case(
            FileParser::instance()->getConfigVariables($this->templateElementsPath . $template . '.htm')
        );

        if (!empty($vars) && isset($vars['name'])) {
            return $vars['name'] . ' (' . $template . ')';
        }

        return $template;
    }

    /**
     * @return string
     * @throws FileNotFoundException
     */
    public function getMarkupAttribute(): string
    {
        if (!$this->template) {
            return '';
        }
        $file = FileParser::instance()->getFileContent($this->templateElementsPath . $this->template . '.htm');

        return $file;
    }

    /**
     * @return array|false
     */
    public function getSettingsAttribute()
    {
        if (!$this->template) {
            return false;
        }

        return FileParser::instance()->getTwigSettings('elements/' . $this->template . '.htm');
    }

    /**
     * Render html
     *
     * @return string
     * @throws CmsException
     */
    public function renderHTML($model = null): string
    {
        if ($this->content_html && $this->active) {
            if (Settings::get('cache_active', true)) {
                return rawurldecode($this->content_html);
            }

            return rawurldecode($this->renderHTMLNoCache($model));
        }

        return '';
    }

    /**
     * Render html without cache
     *
     * @return string
     * @throws CmsException
     */
    protected function renderHTMLNoCache($model = null): string
    {
        if ($this->page->theme) {
            FileParser::instance()->setTheme($this->page->theme);
        }
        $content = $this->content ?? []; //  ["project_tag"]=> string(6) "{name}"

        if ($this->page && $model !== null) {
            $contentModel = $model->toArray() ?? [];  //  ["name"]=> string(6) "NAme"
            foreach ($content as $key => $value) {
                if ($value !== '' && !empty($value) && isset($contentModel[str_replace(['{', '}'], '', $value)])) {
                    $content[$key] = $contentModel[str_replace(['{', '}'], '', $value)];
                }
            }
        }

        if ($this->template && $this->template !== 'HTML') {
            if ($this->settings && isset($this->settings['twig'])) {
                $twigHtml = FileParser::instance()->renderPartial(
                    'elements/' . $this->template . '.htm',
                    $content
                );
                $html = SyntaxParser::parse($twigHtml);

                return html_entity_decode($html->render($content));
            }

            $parsed = SyntaxParser::parse($this->markup);

            return $parsed->render($content);
        }

        return $this->content['html'] ?? '';
    }

    /**
     * Clear cache
     *
     * @return string
     */
    public function clearCache(): string
    {
        if ($this->content_html && $this->active) {
            return rawurldecode($this->content_html);
        }

        return '';
    }
}
