<?php namespace NextLevels\NextLevelCms\Events;

use Backend\Widgets\Form;
use NextLevels\NextLevelCms\Models\Element;
use October\Rain\Parse\Syntax\Parser as SyntaxParser;

class BackendFormFields
{

    /**
     * Extend backend form fields
     *
     * @param Form $form
     */
    public static function extendFields(Form $form): void
    {
        if (!$form->model instanceof Element) {
            return;
        }

        if ($form->model->template !== null) {
            $syntax = SyntaxParser::parse($form->model->template->markup);
            $form = [
                'fields' => $syntax->toEditor()
            ];
        }
    }
}
