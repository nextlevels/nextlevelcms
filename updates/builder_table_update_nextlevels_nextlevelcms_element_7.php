<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsElement7 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->mediumText('content_html');
            $table->string('name', 255)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->dropColumn('content_html');
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
