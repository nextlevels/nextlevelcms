<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsNextlevelcmsMenuPages extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_nextlevelcms_menu_pages', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('m_id');
            $table->integer('p_id');
            $table->primary(['m_id', 'p_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_nextlevelcms_menu_pages');
    }
}
