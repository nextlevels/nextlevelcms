<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsElemente1 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->mediumText('content_html')->nullable(true)->change();
            $table->mediumText('content_editor')->nullable(true)->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->mediumText('content_html')->nullable(false)->change();
            $table->mediumText('content_editor')->nullable(false)->change();
        });
    }
}
