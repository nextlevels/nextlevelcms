<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsPage extends Migration
{
    public function up()
    {
        Schema::rename('nextlevels_nextlevelcms_pages', 'nextlevels_nextlevelcms_page');
    }

    public function down()
    {
        Schema::rename('nextlevels_nextlevelcms_page', 'nextlevels_nextlevelcms_pages');
    }
}
