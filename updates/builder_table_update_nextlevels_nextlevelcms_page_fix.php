<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsPageFix extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->text('content_html')->nullable();
            $table->renameColumn('content', 'content_editor');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->dropColumn('content_html');
            $table->renameColumn('content_editor', 'content');
        });
    }
}
