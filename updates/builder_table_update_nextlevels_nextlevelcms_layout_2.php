<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsLayout2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_layout', function ($table) {
            $table->string('model_class')->nullable();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_layout', function ($table) {
            $table->dropColumn('model_class');
        });
    }
}
