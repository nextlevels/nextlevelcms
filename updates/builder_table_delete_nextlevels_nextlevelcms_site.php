<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableDeleteNextlevelsNextlevelcmsSite extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nextlevels_nextlevelcms_site');
    }

    public function down()
    {
        Schema::create('nextlevels_nextlevelcms_site', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->string('domain', 191);
            $table->string('theme', 191)->nullable();
        });
    }
}
