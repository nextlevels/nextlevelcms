<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsNextlevelcmsElement extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_nextlevelcms_element', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->boolean('active')->default(1);
            $table->text('content');
            $table->integer('parent')->nullable();
            $table->integer('sort')->default(1);
            $table->string('name', 255)->nullable();
            $table->integer('element_template_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_nextlevelcms_element');
    }
}
