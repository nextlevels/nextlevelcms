<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsPage10 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->string('seo_visibility', 191)->nullable()->default('all');
            $table->boolean('active')->default(1)->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->dropColumn('seo_visibility');
            $table->boolean('active')->default(0)->change();
        });
    }
}
