<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsPage16 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->integer('data_object_id')->nullable();
            $table->string('data_object_model', 191)->nullable();
            $table->string('css_classes', 191)->default(null)->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->dropColumn('data_object_id');
            $table->dropColumn('data_object_model');
            $table->string('css_classes', 191)->default(null)->change();
        });
    }
}
