<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsElement8 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->mediumText('content_editor');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->dropColumn('content_editor');
        });
    }
}
