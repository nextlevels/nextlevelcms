<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsElementTemplate extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_element_template', function ($table) {
            $table->renameColumn('fields', 'content');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_element_template', function ($table) {
            $table->renameColumn('content', 'fields');
        });
    }
}
