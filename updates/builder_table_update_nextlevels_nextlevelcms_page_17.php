<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsPage17 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->integer('layout_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->dropColumn('layout_id');
        });
    }
}
