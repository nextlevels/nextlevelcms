<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsElement14 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->boolean('editor_save')->default(0);
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->dropColumn('editor_save');
        });
    }
}
