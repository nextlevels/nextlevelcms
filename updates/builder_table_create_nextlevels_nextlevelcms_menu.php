<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsNextlevelcmsMenu extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_nextlevelcms_menu', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_nextlevelcms_menu');
    }
}
