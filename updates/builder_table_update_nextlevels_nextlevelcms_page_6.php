<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsPage6 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->string('content')->nullable();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->dropColumn('content');
        });
    }
}
