<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableDeleteNextlevelsNextlevelcmsElementTemplate extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nextlevels_nextlevelcms_element_template');
    }

    public function down()
    {
        Schema::create('nextlevels_nextlevelcms_element_template', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->text('content');
            $table->text('markup');
        });
    }
}
