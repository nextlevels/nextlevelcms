<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsMenu2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_menu', function ($table) {
            $table->string('code')->nullable();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_menu', function ($table) {
            $table->dropColumn('code');
        });
    }
}
