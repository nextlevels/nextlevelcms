<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsMenuPages extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_menu_pages', function ($table) {
            $table->integer('sort')->nullable()->default(0);
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_menu_pages', function ($table) {
            $table->dropColumn('sort');
        });
    }
}
