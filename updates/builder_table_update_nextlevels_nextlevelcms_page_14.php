<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsPage14 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->integer('parent_id')->nullable()->unsigned();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_page', function ($table) {
            $table->dropColumn('parent_id');
        });
    }
}
