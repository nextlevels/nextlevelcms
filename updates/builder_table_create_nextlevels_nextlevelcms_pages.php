<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsNextlevelcmsPages extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_nextlevelcms_pages', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->string('slug', 191);
            $table->integer('layout');
            $table->boolean('active');
            $table->string('seo_title', 191)->nullable();
            $table->text('seo_description')->nullable();
            $table->string('seo_keywords')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_nextlevelcms_pages');
    }
}
