<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsSite extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_site', function ($table) {
            $table->string('theme')->nullable();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_site', function ($table) {
            $table->dropColumn('theme');
        });
    }
}
