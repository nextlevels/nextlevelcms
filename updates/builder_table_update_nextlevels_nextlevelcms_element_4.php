<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsElement4 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->text('content')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->text('content')->nullable(false)->change();
        });
    }
}
