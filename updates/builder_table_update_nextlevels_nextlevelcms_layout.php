<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsLayout extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_layout', function ($table) {
            $table->integer('page_id')->nullable()->unsigned();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_layout', function ($table) {
            $table->dropColumn('page_id');
        });
    }
}
