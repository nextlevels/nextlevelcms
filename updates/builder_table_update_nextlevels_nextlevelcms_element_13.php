<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsNextlevelcmsElement13 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->integer('sort_order')->nullable()->change();
            $table->string('name', 255)->nullable()->change();
            $table->dropColumn('nest_left');
            $table->dropColumn('nest_right');
            $table->dropColumn('nest_depth');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_nextlevelcms_element', function ($table) {
            $table->integer('sort_order')->nullable(false)->change();
            $table->string('name', 255)->nullable(false)->change();
            $table->smallInteger('nest_left')->default(0);
            $table->smallInteger('nest_right')->default(0);
            $table->smallInteger('nest_depth')->default(0);
        });
    }
}
