<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsNextlevelcmsSite extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_nextlevelcms_site', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('domain');
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_nextlevelcms_site');
    }
}
