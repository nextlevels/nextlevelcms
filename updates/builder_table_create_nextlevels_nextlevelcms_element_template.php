<?php namespace NextLevels\NextLevelCms\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsNextlevelcmsElementTemplate extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_nextlevelcms_element_template', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->text('fields');
            $table->text('markup');
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_nextlevelcms_element_template');
    }
}
