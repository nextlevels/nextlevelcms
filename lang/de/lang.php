<?php return [
    'plugin' => [
        'name' => 'Next Level CMS',
        'description' => ''
    ],
    'models' => [
        'element' => [
            'label' => 'Element',
            'label_multi' => 'Elemente',
            'template' => 'Template',
            'name' => 'Beschreibung',
            'content' => 'Inhalt',
            'element_template_id' => 'element_template_id',
            'tabs' => [
                'pages' => 'Seiten'
            ]
        ],
        'layout' => [
            'label' => 'Layout',
            'label_multi' => 'Layouts',
            'name' => 'Name'
        ],
        'menu' => [
            'label' => 'Menü',
            'label_multi' => 'Menüs',
            'name' => 'Name',
            'code' => 'Code',
            'number_of_pages' => 'Anzahl Seiten',
        ],
        'page' => [
            'label' => 'Seite',
            'label_multi' => 'Seiten',
            'name' => 'Name',
            'slug' => 'URL',
            'pivot_sort' => 'Reihenfolge',
            'parent' => [
                'label' => 'Oberseite',
                'emptyOption' => 'Keine Oberseite'
            ],
            'seo_title' => 'Titel',
            'seo_visibility' => 'Sichtbarkeit',
            'seo_visibility_options' => [
                'all' => 'Indexiert',
                'noindex' => 'Diese Seite nicht in den Suchergebnissen anzeigen.',
                'nofollow' => 'Den Links auf dieser Seite nicht folgen.',
                'noimageindex' => 'Bilder auf dieser Seite nicht indexieren.',
                'nosnippet' => 'Kein Text-Snippet und keine Videovorschau.',
            ],
            'seo_description' => 'Beschreibung',
            'data_layout' => 'Daten Layout',
            'seo_description_comment' => 'Bei einer Optimierung werden deshalb empfohlen, um die 150 Wörter in der Meta Description zu verwenden.',
            'seo_thumbnail' => 'Vorschaubild',
            'seo_thumbnail_comment' => 'Hier tragen Sie die URL eines Bildes an, das beim Teilen oder Liken Ihrer Seite angezeigt werden soll. Das ausgewählte Bild muss mindestens 600x315px, ideal sind 1200x630px.',
            'seo_keywords' => 'Keywords',
            'seo_keywords_comment' => 'Wird als interne Klassifizierung verwendet und  nicht als Meta Tag.',
            'type' => 'Typ',
            'type_options' => [
                'cms_page' => 'CMS Seite',
                'theme_page' => 'Theme Seite',
                'redirect' => 'Weiterleitung',
            ],
            'redirect_link' => 'Weiterleitung',
            'redirect_link_comment' => 'Achtung: Wenn dieses Feld gesetzt ist greifen alle weiteren Seiteneinstellungen nicht.',
            'tabs' => [
                'elements' => 'Elemente',
                'seo' => 'SEO',
                'settings' => 'Einstellungen',
                'page' => 'Seite'
            ]
        ],
        'settings' => [
            'cache_active' => 'Cache aktivieren'
        ],
        'site' => [
            'label' => 'SiteFront',
            'label_multi' => 'SiteFronts',
            'name' => 'Name',
            'domain' => 'Domain',
            'theme' => 'Theme',
            'tabs' => [
                'pages' => 'Seiten'
            ]
        ]
    ],
    'fields' => [
        'id' => 'ID',
        'active' => 'Aktiv?',
        'sort_order' => 'Sortierung',
        'created_at' => 'Erstellt am',
        'updated_at' => 'Geändert am'
    ],
    'tabs' => [
        'general' => 'Allgemein'
    ],
    'list' => [
        'clone_selected' => 'Markierte duplizieren',
        'clone_selected_confirm' => 'Markierte Einträge duplizieren?',
        'clone_selected_empty' => 'Es wurden keine markierten Elemente gefunden.',
        'clone_selected_success' => 'Es wurden %s Elemente dupliziert.',
    ],
    'settings' => [
        'settings' => [
            'label' => 'Cache Einstellungen',
            'description' => 'Cache Einstellungen',
            'category' => 'CMS'
        ]
    ],
    'permissions' => [
        'nextlevels_cms' => [
            'label' => 'Seiten verwalten',
            'create_pages' => 'Neue Seiten anlegen',
            'duplicate_pages' => 'Seiten duplizieren',
            'delete_pages' => 'Seiten löschen',
            'tab' => 'Next CMS',
            'errors' => [
                'no_permission_create_pages' => 'Sie haben keine Berechtigung, um neue Seiten anzulegen.',
            ]
        ]
    ],
    1 => 'CMS Seite',
];
