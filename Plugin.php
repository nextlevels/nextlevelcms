<?php

namespace NextLevels\NextLevelCms;

use Backend\Classes\Controller;
use Config;
use Event;
use Model;
use NextLevels\NextLevelCms\Classes\PageHandler;
use NextLevels\NextLevelCms\Components\NextBlogPosts;
use NextLevels\NextLevelCms\Components\NextCMS;
use NextLevels\NextLevelCms\Events\BackendFormFields;
use NextLevels\NextLevelCms\Models\Element;
use NextLevels\NextLevelCms\Models\Menu;
use NextLevels\NextLevelCms\Models\Page;
use NextLevels\NextLevelCms\Models\Settings;
use Session;
use System\Classes\PluginBase;

/**
 * Class Plugin
 */
class Plugin extends PluginBase
{

    /**
     * Boot
     */
    public function boot(): void
    {
        Config::set('filesystems.disks.root', [
            'driver' => 'local',
            'root' => public_path()
        ]);
        Element::observe(Observers\ElementObserver::class);
        Page::observe(Observers\PageObserver::class);

        Model::extend(function ($model) {
            $model->bindEvent('model.afterSave', function () use ($model) {
                if ($model->update) {
                    $elements = Element::where('template', $model->update)->get();

                    if ($elements) {
                        foreach ($elements as $element) {
                            $element->save();
                        }
                    }
                }
            });
        });

        Event::listen('form.extendFieldsBefore', [BackendFormFields::class, 'extendFields']);

        Event::listen('cms.router.beforeRoute', function ($url) {
            return PageHandler::instance()->getPage($url);
        });

        Event::listen('cms.page.beforeRenderPage', function ($controller, $page) {
            /*
             * Before twig renders
             */
            $twig = $controller->getTwig();
            $loader = $controller->getLoader();
            PageHandler::instance()->injectPageTwig($page, $loader, $twig);

            /*
             * Get rendered content
             */
            $contents = PageHandler::instance()->getPageContents($page);

            if ($contents !== '') {
                return $contents;
            }
        });

        Event::listen('backend.page.beforeDisplay', function (Controller $controller, $action, $params) {
            if (in_array($action, ['create', 'update'])) {
                Element::extend(function (Element $element) {
                    $element->bindEvent('model.beforeFetch', function () use ($element) {
                        if (($lang = Session::get('lang')) !== null) {
                            $element->translateContext($lang);
                        }
                    });
                });

                Page::extend(function (Page $page) {
                    $page->bindEvent('model.beforeFetch', function () use ($page) {
                        if (($lang = Session::get('lang')) !== null) {
                            $page->translateContext($lang);
                        }
                    });
                });

                Menu::extend(function (Menu $menu) {
                    $menu->bindEvent('model.beforeFetch', function () use ($menu) {
                        if (($lang = Session::get('lang')) !== null) {
                            $menu->translateContext($lang);
                        }
                    });
                });
            }
        });
    }

    /**
     * @return array
     */
    public function registerPermissions(): array
    {
        return [
            'nextlevels.cms' => [
                'label' => 'nextlevels.nextlevelcms::lang.permissions.nextlevels_cms.label',
                'tab' => 'nextlevels.nextlevelcms::lang.permissions.nextlevels_cms.tab',
                'order' => 200,
            ],
            'nextlevels.cms.create_pages' => [
                'label' => 'nextlevels.nextlevelcms::lang.permissions.nextlevels_cms.create_pages',
                'tab' => 'nextlevels.nextlevelcms::lang.permissions.nextlevels_cms.tab',
                'order' => 200,
            ],
            'nextlevels.cms.duplicate_pages' => [
                'label' => 'nextlevels.nextlevelcms::lang.permissions.nextlevels_cms.duplicate_pages',
                'tab' => 'nextlevels.nextlevelcms::lang.permissions.nextlevels_cms.tab',
                'order' => 200,
            ],
            'nextlevels.cms.delete_pages' => [
                'label' => 'nextlevels.nextlevelcms::lang.permissions.nextlevels_cms.delete_pages',
                'tab' => 'nextlevels.nextlevelcms::lang.permissions.nextlevels_cms.tab',
                'order' => 200,
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerComponents(): array
    {
        return [
            NextBlogPosts::class => 'NextBlogPosts',
            NextCMS::class => 'NextCMS',
        ];
    }

    /**
     * @return array
     */
    public function registerSettings(): array
    {
        return [
            'settings' => [
                'label' => 'nextlevels.nextlevelcms::lang.settings.settings.label',
                'description' => 'nextlevels.nextlevelcms::lang.settings.settings.description',
                'category' => 'nextlevels.nextlevelcms::lang.settings.settings.category',
                'icon' => 'icon-paper-plane',
                'class' => Settings::class,
                'order' => 23,
                'keywords' => 'cms settings'
            ]
        ];
    }
}
