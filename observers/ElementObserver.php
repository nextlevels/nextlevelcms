<?php namespace NextLevels\NextLevelCms\Observers;

use Cms\Classes\CmsException;
use NextLevels\NextLevelCms\Classes\FileParser;
use NextLevels\NextLevelCms\Models\Element;
use October\Rain\Parse\Syntax\Parser as SyntaxParser;

/**
 * Class ElementObserver
 */
class ElementObserver
{

    /**
     * Listen to Site saving event
     *
     * @param Element $element
     *
     * @throws CmsException
     */
    public function saving(Element $element): void
    {
        if (isset($element->page->theme)) {
            FileParser::instance()->setTheme($element->page->theme);
        }

        if ($element->template && $element->template != 'HTML') {
            if ($element->settings && isset($element->settings['twig'])) {
                $TwigHtml = FileParser::instance()->renderPartial(
                    'elements/' . $element->template . '.htm',
                    $element->content
                );
                $html = SyntaxParser::parse($TwigHtml);
                $element->content_html = html_entity_decode($html->render($element->content));
            } else {
                $parsed = SyntaxParser::parse($element->markup);
                $element->content_html = $parsed->render($element->content);
            }
        } else {
            if ($element->editor_save) {
                $element->editor_save = 0;
            } elseif (isset($element->content['html'])) {
                $element->content_html = $element->content['html'];
            }
        }
    }
}
