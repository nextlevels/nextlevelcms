<?php namespace NextLevels\NextLevelCms\Observers;

use NextLevels\NextLevelCms\Models\Page;

class PageObserver
{
    /**
     * Listen to Site deleting event
     *
     * @param Page $page
     */
    public function deleted(Page $page): void
    {
        $page->elements()->delete();
    }
}
