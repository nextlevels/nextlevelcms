/**
 * initializeSorting()
 *
 * This sets up our list to interact with html5sortable. It is called
 * on page load, and after the list is refreshed.
 */
function initializeSorting() {

    // Disable our toolbar buttons
    $("[data-control='toolbar']").find('button').prop('disabled', true);

    // Add a sortable class to our table so our css can easily distinguish
    // sortable / non-sortable lists.
    $('.list-widget').addClass('html5sortable');

    // Add a "disabled" class to rows that should not be sorted
    $('.list-widget tr:has(.disabled)').addClass('disabled');

    sortable('.list-widget tbody', {
        items: ':not(.disabled)',
        forcePlaceholderSize: true,
    });

    // Callback function for when sorting is completed
    $('.list-widget tbody').sortable().bind('sortupdate', function (e) {

        $('.list-widget tbody tbody tr').css('display', '');

        $.request('onUpdatePosition', {
            data: {
                checked: $('.index_reposition').map(function () {
                    return $(this).val()
                }).get(),
                id: $("#sortId").data('id') ?? 1,
            },
            success: function () {

            },
        });

    });
}

/**
 * initializeSwitcher()
 *
 */
function initializeSwitcher() {
    $('.bool-column-list:checkbox').change(function () {
        $.request('onUpdateBool', {
            data: {
                checked: $(this).is(":checked") ? "checked" : "notchecked",
                id: $(this).data('id'),
                action: $(this).data('action'),
            },
            success: function () {
            },
        });
    });
}


jQuery(function () {
    initializeSorting();
    initializeSwitcher();
    /*
      $('a[data-master-locale-switch]').click(function () {
       $.request('onUpdateLang', {
              data: {
                  lang: $(this).data('master-locale-switch'),
                  id: $(this).data('id'),
              },
              success: function() {
              },
          });
    });*/

    $('.bool-column-list:checkbox').change(function () {
        $.request('onUpdateLang', {
            data: {
                lang: $(this).data('master-locale-switch'),
                id: $(this).data('id'),
            },
            success: function () {
            },
        });
    });

});

// Solve noscroll on double modal
$(document).ready(function () {
    $('body').on('hidden.bs.modal', function () {

        if ($('.modal.in').length > 0) {
            setTimeout(
                function () {

                    $('body').addClass('modal-open');
                }, 500);

        }
    });
});
