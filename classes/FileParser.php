<?php namespace NextLevels\NextLevelCms\Classes;

use Cms\Classes\CmsException;
use Cms\Classes\Controller as FrontendController;
use Cms\Classes\Partial;
use Cms\Classes\Theme;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;
use Ini;
use Lang;
use Log;
use October\Rain\Support\Traits\Singleton;
use SystemException;

/**
 * Class FileParser
 */
class FileParser
{
    use Singleton;

    /**
     * @var Theme
     */
    protected $theme;

    /**
     * @var string
     */
    protected $themeCode;

    /**
     * @var string
     */
    protected $themeDir;

    /**
     * @var FrontendController
     */
    protected $frontendController;

    /**
     * Set given theme
     *
     * @param $theme
     *
     * @throws CmsException
     */
    public function setTheme($theme): void
    {
        $this->theme = Theme::load($theme);

        if (!$this->theme) {
            throw new CmsException(Lang::get('cms::lang.theme.active.not_found'));
        }

        $this->themeCode = $theme;
        $this->themeDir = 'themes/' . $theme;
        $this->frontendController = new FrontendController(Theme::load($theme));
    }

    /**
     * Get twig settings
     *
     * @param string $filename
     *
     * @return false|array
     */
    public function getTwigSettings(string $filename)
    {
        $partials = Partial::listInTheme($this->theme);
        $partialKey = $partials->search(function ($item, $key) use ($filename) {
            return $item->getFileName() === $filename;
        });

        if ($partials[$partialKey] !== null) {
            return $partials[$partialKey]->settings;
        }

        return false;
    }

    /**
     * Get file content
     *
     * @param string $filename
     *
     * @return string
     * @throws FileNotFoundException
     */
    public function getFileContent(string $filename): string
    {
        return Storage::disk('root')->get($this->themeDir . $filename);
    }

    /**
     * Get config variables from file
     *
     * @param string $filename
     *
     * @return array
     */
    public function getConfigVariables(string $filename): array
    {
        if (file_exists($this->themeDir . $filename)) {
            try {
                return Ini::parse(explode('==', file_get_contents($this->themeDir . $filename))[0]);
            } catch (Exception $exception) {
                Log::error($exception->getMessage());
            }
        }

        return [];
    }

    /**
     * Get list of files from given path
     *
     * @param string $path
     *
     * @return array
     */
    public function getFileList(string $path): array
    {
        $files = Storage::disk('root')->files($this->themeDir . $path);
        $files = array_map(function ($value) {
            return pathinfo($value, PATHINFO_FILENAME);
        }, $files);

        return array_combine($files, $files);
    }

    /**
     * Get list of shares files from given path
     *
     * @param string $path
     *
     * @return array
     */
    public function getFileListShared(string $path): array
    {
        $layouts = Storage::disk('root')->files('theme-assets/' . $path);
        $layouts = array_map(function ($value) {
            return pathinfo($value, PATHINFO_FILENAME);
        }, $layouts);

        return array_combine($layouts, $layouts);
    }

    /**
     * Render partial
     *
     * @param string $filename
     * @param array|null $data
     *
     * @return false|mixed
     * @throws CmsException
     */
    public function renderPartial(string $filename, array $data = null)
    {
        if ($data === null) {
            return $this->frontendController->renderPartial($filename);
        }

        return $this->frontendController->renderPartial($filename, $data);
    }

    /**
     * Initialize this singleton.
     *
     * @throws CmsException
     * @throws SystemException
     */
    protected function init(): void
    {
        if (($this->theme = Theme::getActiveTheme()) === null) {
            throw new CmsException(Lang::get('cms::lang.theme.active.not_found'));
        }

        $this->themeCode = Theme::getActiveThemeCode();
        $this->themeDir = 'themes/' . Theme::getActiveThemeCode();
        $this->frontendController = new FrontendController(Theme::load(Theme::getActiveTheme()->getDirName()));
    }
}
