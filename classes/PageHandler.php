<?php namespace NextLevels\NextLevelCms\Classes;

use Cms\Classes\CmsException;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Config;
use Db;
use Lang;
use NextLevels\NextLevelCms\Models\Page as CMSPage;
use October\Rain\Database\Builder;
use October\Rain\Support\Traits\Singleton;
use Session;

class PageHandler
{
    use Singleton;

    /**
     * @var Theme
     */
    protected $theme;

    /**
     * Get page by given url
     *
     * @param string $url
     *
     * @return Page|null
     */
    public function getPage(string $url): ?Page
    {
        if (strlen($url) > 1) {
            $url = substr($url, 1);
        }

        $locale = 'de';
        if (Session::get('rainlab.translate.locale')) {
            $locale = Session::get('rainlab.translate.locale');
        }

        $translated_slug = null;
        $slug = null;

        if ($locale !== 'de') {
            $translateIndexes = Db::table('rainlab_translate_attributes')
                ->where('rainlab_translate_attributes.model_type', '=', 'Nextlevels\NextLevelCms\Models\Page')
                ->where('rainlab_translate_attributes.locale', '=', $locale)
                ->where('rainlab_translate_attributes.attribute_data', 'LIKE', "%{$url}%")
                ->pluck('attribute_data');

            $translateModel = Db::table('rainlab_translate_attributes')
                ->where('rainlab_translate_attributes.model_type', '=', 'Nextlevels\NextLevelCms\Models\Page')
                ->where('rainlab_translate_attributes.locale', '=', $locale)
                ->where('rainlab_translate_attributes.attribute_data', 'LIKE', "%{$url}%")
                ->pluck('model_id');

            if (isset($translateIndexes[0])) {
                $translated_slug = json_decode($translateIndexes[0])->slug;
                $originalPage = CMSPage::where('id', $translateModel[0])->first();
                $slug = $originalPage->attributes['slug'];
            }
        }

        if ($locale !== 'de') {
            if ($translated_slug === $url) {
                $url = $slug;
            }
        }

        $originalUrl = $url;
        $url = explode('/', $url)[0];
        $cmsPages = CMSPage::where('slug', $url)
            ->where(static function (Builder $builder) {
                $builder
                    ->whereHas('site', static function (Builder $builder) {
                        $builder->where('domain', $_SERVER['HTTP_HOST']);
                    })
                    ->orWhereNull('site_id');
            })
            ->get();

        if ($cmsPages->isEmpty()) {
            return null;
        }

        $cmsPage = $cmsPages->first();

        if ($cmsPage->type === 2) {
            if (Theme::getActiveTheme()->getDirName() != $cmsPage->site->theme) {
                Theme::setActiveTheme($cmsPage->site->theme);
                $systemPage = Page::inTheme(Theme::load($cmsPage->site->theme));
                header('Location: ' . 'https://' . $cmsPage->site->domain . '/' . $cmsPage->slug);
                die();
            }
            return null;
        }

        if ($cmsPage->redirect_link !== '' && $cmsPage->redirect_link !== null) {
            header('Location: ' . $cmsPage->redirect_link);
            die();
        }

        Theme::setActiveTheme(Theme::getActiveTheme()->getDirName());
        $systemPage = Page::inTheme(Theme::load(Theme::getActiveTheme()->getDirName()));

        if ($cmsPage->site) {
            Theme::setActiveTheme($cmsPage->site->theme);
            $systemPage = Page::inTheme(Theme::load($cmsPage->site->theme));
        }

        $systemPage->url = $originalUrl;
        $systemPage->layout = 'default';
        $systemPage->settings['layout'] = $cmsPage->layout;
        $systemPage->apiBag['pageData'] = $cmsPage;
        $systemPage->settings['id'] = $cmsPage->id;

        return $systemPage;
    }

    /**
     * Inject page twig
     *
     * @param $page
     * @param $loader
     * @param $twig
     */
    public function injectPageTwig($page, $loader, $twig)
    {
        if (!isset($page->apiBag['pageData'])) {
            return;
        }
    }

    /**
     * Get page content
     *
     * @param $page
     *
     * @return mixed|void
     */
    public function getPageContents($page)
    {
        if (!isset($page->apiBag['pageData'])) {
            return;
        }
        $cmsPage = $page->apiBag['pageData'];
        $page->apiBag['model'] = null;

        if ($cmsPage->data_layout) {
            $url = explode('/', $page->url);
            if (count($url) > 1) {
                $model = null;
                $modelClass = ucwords($cmsPage->data_layout->model_class);
                if (class_exists($modelClass)) {
                    $modelClass = new $modelClass();
                    $model = $modelClass->where('slug', $url[1])->first();
                    $cmsPage->name = $model->name;
                    $cmsPage->seo_title = $model->name;
                    $cmsPage->seo_description = $model->name;
                    $page->apiBag['model'] = $model;
                }
            }
        }

        $page->title = ($cmsPage->seo_title != '') ? $cmsPage->seo_title : $cmsPage->name;
        $page->meta_description = $cmsPage->seo_description;
        $page->meta_title = ($cmsPage->seo_title != '') ? $cmsPage->seo_title : $cmsPage->name;
        $page->seo_visibility = $cmsPage->seo_visibility;
        $page->seo_thumbnail = Config::get('cms.storage.media.path') . $cmsPage->seo_thumbnail;

        return $page->apiBag['pageData']->renderHTML($page->apiBag['model']);
    }

    /**
     * @throws CmsException
     */
    protected function init(): void
    {
        $this->theme = Theme::getActiveTheme();

        if (!$this->theme) {
            throw new CmsException(Lang::get('cms::lang.theme.active.not_found'));
        }
    }
}
