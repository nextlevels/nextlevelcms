<?php

return [
    'stylefiles' => [
        'https://use.typekit.net/tsm0nre.css',
        '/theme-assets/assets/css/app.css',
        '/modules/system/assets/css/framework.extras.css'
    ]
];
