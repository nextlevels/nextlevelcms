<?php

namespace NextLevels\NextLevelCms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class LayoutController extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('NextLevels.NextLevelCms', 'next-cms-main', 'next-cms-main-layouts');
    }

    /**
     * lang action
     *
     * @return RedirectResponse
     */
    public function lang($lang): RedirectResponse
    {
        if (
            \BackendAuth::getUser()
        ) {
            \Session::put('locale', $lang);
            \App::setLocale($lang);
        }

        return Redirect::back();
    }
}
