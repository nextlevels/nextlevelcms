<?php

namespace NextLevels\NextLevelCms\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Behaviors\RelationController;
use Backend\Classes\Controller;
use BackendMenu;
use Cms\Classes\CmsException;
use Cms\Classes\Theme;
use Flash;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Lang;
use NextLevels\NextLevelCms\Behaviors\AjaxSortController;
use NextLevels\NextLevelCms\Classes\FileParser;
use NextLevels\NextLevelCms\Helpers\BaseHelper;
use NextLevels\NextLevelCms\Models\Element;
use NextLevels\NextLevelCms\Models\Page;
use NextLevels\NextLevelCms\Models\Page as CMSPage;
use October\Rain\Database\Collection;
use October\Rain\Exception\ApplicationException;
use October\Rain\Parse\Syntax\Parser as SyntaxParser;
use Request;
use Session;
use Storage;

/**
 * Class PagesController
 */
class PagesController extends Controller
{

    /**
     * @var string[]
     */
    public $requiredPermissions = ['nextlevels.cms'];

    /**
     * @var string[] behaviors
     */
    public $implement = [
        ListController::class,
        FormController::class,
        RelationController::class,
        AjaxSortController::class
    ];

    /**
     * @var string behavior config files
     */
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    /**
     * PagesController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('NextLevels.NextLevelCms', 'next-cms-main', 'next-cms-main-pages');
    }

    /**
     * Bulk clone records.
     *
     * @throws ApplicationException when the parent definition is missing.
     */
    public function index_onClone()
    {
        $listConfig = $this->listGetConfig();

        /*
         * Validate checked identifiers
         */
        $checkedIds = post('checked');

        if (!$checkedIds || !is_array($checkedIds) || !count($checkedIds)) {
            Flash::error(Lang::get('nextlevels.nextlevelcms::lang.list.clone_selected_empty'));

            return $this->listRefresh();
        }

        /*
         * Create the model
         */
        $class = $listConfig->modelClass;
        $model = new $class;
        $model = $this->listExtendModel($model);

        /*
         * Create the query
         */
        $query = $model->newQuery();
        $this->listExtendQueryBefore($query);

        $query->whereIn($model->getKeyName(), $checkedIds);
        $this->listExtendQuery($query);

        /*
         * Delete records
         */
        $records = $query->get();
        $recordsCount = $records->count();

        if ($recordsCount > 0) {
            foreach ($records as $record) {
                $replicatedRecord = $record->replicate();
                $replicatedRecord->name .= ' Copy';
                $replicatedRecord->save();

                foreach ($record->elements as $element) {
                    $replicatedRecord->elements()->create($element->toArray());
                }
            }

            Flash::success(sprintf(
                Lang::get('nextlevels.nextlevelcms::lang.list.clone_selected_success'),
                $recordsCount
            ));
        } else {
            Flash::error(Lang::get('nextlevels.nextlevelcms::lang.list.clone_selected_empty'));
        }

        return $this->listRefresh();
    }

    public function create()
    {
        if (!$this->user->hasAccess('nextlevels.cms.create_pages')) {
            Flash::error(Lang::get('nextlevels.nextlevelcms::lang.permissions.nextlevels_cms.errors.no_permission_create_pages'));

            return Redirect::to(\Backend::url('nextlevels/nextlevelcms/pagescontroller'));
        }

        parent::create();
    }

    /**
     * Extend update function
     *
     * @param int|null $recordId
     * @param string|null $context
     *
     * @throws CmsException
     */
    public function update(int $recordId = null, string $context = null): void
    {
        parent::update($recordId, $context);

        if (($page = Page::find($recordId)) !== null && $page->site !== null) {
            FileParser::instance()->setTheme($page->site->theme);
        }
    }

    /**
     * @return Collection
     */
    public function getTemplateFields(): Collection
    {
        return ElementTemplate::all();
    }

    /**
     * @return Collection
     */
    public function getPageElements(): Collection
    {
        return $this->formGetModel()->elements()->with('template')->get();
    }

    /**
     * Design action
     *
     * @param int $id
     *
     * @throws FileNotFoundException
     */
    public function design(int $id): void
    {
        if (($page = CMSPage::find($id)) !== null) {
            $blocks = [];
            $this->vars['model'] = $page;
            $this->vars['content_editor'] = $page->content_editor;
            $this->vars['content_html'] = $page->content_html;
            $blockFiles = Storage::disk('root')
                ->files('/themes/' . Theme::getActiveTheme()->getDirName() . '/blocks/');

            foreach ($blockFiles as $file) {
                $blocks[pathinfo($file, PATHINFO_FILENAME)] = BaseHelper::removeHtmlComments(utf8_encode(
                    Storage::disk('root')->get($file)
                ));
            }

            $this->vars['blocks'] = json_encode($blocks);
        }
    }

    /**
     * Store action
     *
     * @param int $id
     */
    public function store(int $id): void
    {
        if (($page = CMSPage::find($id)) !== null) {
            $page->content_editor = Request::input();
            $page->content_html = Request::input()['gjs-html'];
            $page->save();
        }
    }

    /**
     * Extend relation manage widget
     *
     * @param $widget
     * @param $field
     * @param $model
     */
    public function relationExtendManageWidget($widget, $field, $model): void
    {
        $syntax = null;

        if (!$widget->model instanceof Element || $field !== 'elements') {
            return;
        }

        if (
            $widget->model->template !== null
            && ($syntax = SyntaxParser::parse($widget->model->markup)->toEditor()) !== null
        ) {
            $widget->bindEvent('form.extendFieldsBefore', function () use (&$widget, $syntax) {
                if ($widget->model->template !== null) {
                    $fields = $syntax;
                    $lang = Session::get('lang');

                    if ($lang !== null && ($element = Element::find($widget->model->id)) !== null) {
                        $widget->model = $element->noFallbackLocale()->lang($lang);
                        $widget->model->content = $element->getAttributeTranslated('content', $lang);
                        $widget->model->name = $element->getAttributeTranslated('name', $lang);
                    }

                    foreach ($fields as $fieldCode => $fieldConfig) {
                        if ($fieldConfig['type'] === 'fileupload') {
                            continue;
                        }

                        if ($fieldConfig['type'] === 'repeater') {
                            $fieldConfig['form']['fields'] = array_get($fieldConfig, 'fields', []);
                            unset($fieldConfig['fields']);
                        }

                        $placement = (!empty($fieldConfig['placement']) ? $fieldConfig['placement'] : null);

                        switch ($placement) {
                            case 'primary':
                                $widget->tabs['fields']['content[' . $fieldCode . ']'] = $fieldConfig;
                                break;

                            default:
                                $fieldConfig['cssClass'] = 'secondary-tab ' . array_get($fieldConfig, 'cssClass', '');
                                $widget->secondaryTabs['fields']['content[' . $fieldCode . ']'] = $fieldConfig;
                                break;
                        }
                    }
                }
            });
        }
    }

    /**
     * Cache action
     *
     * @return RedirectResponse
     */
    public function cache(): RedirectResponse
    {
        $pages = CMSPage::all();

        foreach ($pages as &$page) {
            $page->clearCache();
        }

        return Redirect::back();
    }
}
