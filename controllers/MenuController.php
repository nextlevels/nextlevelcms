<?php namespace NextLevels\NextLevelCms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class MenuController extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
        'NextLevels\NextLevelCms\Behaviors\AjaxSortController'
    ];
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('NextLevels.NextLevelCms', 'next-cms-main', 'side-menu-item');
    }
}
