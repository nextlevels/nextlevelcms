<?php namespace NextLevels\NextLevelCms\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Classes\Controller;
use Cms\Classes\Theme;
use Config;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\JsonResponse;
use Log;
use NextLevels\NextLevelCms\Helpers\BaseHelper;
use NextLevels\NextLevelCms\Models\Element;
use October\Rain\Database\Collection;
use Request;
use Response;
use Session;
use Storage;

/**
 * Class ElementController
 */
class ElementController extends Controller
{

    /**
     * @var string[]
     */
    public $requiredPermissions = ['nextlevels.cms'];

    /**
     * @var string[]
     */
    public $implement = [ListController::class, FormController::class];

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    /**
     * @return mixed
     */
    public function getPageElements(): Collection
    {
        return $this->formGetModel()->elements()->with('template')->get();
    }

    /**
     * Extend the list query to position the rows correctly
     *
     * @param      $query
     * @param null $definition
     */
    public function listExtendQuery($query, $definition = null): void
    {
        $query->orderBy('sort_order', 'asc');
    }

    /**
     * Design action
     *
     * @param int $id
     *
     * @throws FileNotFoundException
     */
    public function design(int $id): void
    {
        if (($element = Element::find($id)) !== null) {
            if (($lang = Session::get('lang')) !== null) {
                $element->translateContext($lang);
            }

            $this->vars['model'] = $element;
            $this->vars['content_editor'] = $element->content_editor;
            $this->vars['content_html'] = $element->content['html'];
            $this->vars['content_css'] = $element->content_css;
            $mediaFiles = Storage::disk('root')->files('/storage/app/media/editor/');

            foreach ($mediaFiles as &$file) {
                $file = Config::get('cms.storage.media.path') . '/' . explode('/', $file, 2)[1];
            }

            $this->vars['media'] = json_encode($mediaFiles);
            $blocks = [];

            $blockFiles = Storage::disk('root')
                ->files('/themes/' . Theme::getActiveTheme()->getDirName() . '/blocks/');

            foreach ($blockFiles as &$file) {
                $blocks[pathinfo($file, PATHINFO_FILENAME)] = BaseHelper::removeHtmlComments(
                    utf8_encode(Storage::disk('root')->get($file))
                );
            }

            $partials = [];

            $this->vars['partials'] = json_encode($partials);
            $this->vars['blocks'] = json_encode($blocks);
            $this->vars['stylefiles'] = json_encode(Config::get('nextlevels.nextlevelcms::stylefiles', []));
        }
    }

    /**
     * Store action
     *
     * @param $id
     */
    public function store(int $id): void
    {
        if (($element = Element::find($id)) !== null) {
            if (($lang = Session::get('lang')) !== null) {
                $element->translateContext($lang);
            }

            $element->content_editor = Request::input();
            $element->content_html = Request::input()['gjs-html'];
            $content = $element->content;
            $content['html'] = Request::input()['gjs-html'];
            $element->content = $content;
            $element->editor_save = 1;
            $element->content_css = str_replace(
                '* { box-sizing: border-box; } body {margin: 0;}',
                ' ',
                Request::input()['gjs-css']
            );

            $element->save();
        }
    }

    /**
     * Load action
     *
     * @param int $id
     *
     * @return mixed
     */
    public function load($id)
    {
        $content = [];

        if (($element = Element::find($id)) !== null) {
            $content = $element->content_editor;
        }

        return Response::json($content);
    }

    /**
     * Upload files
     *
     * @return JsonResponse
     */
    public function uploadFiles(): JsonResponse
    {
        $response = [];

        if ($_FILES) {
            $resultArray = [];

            foreach ($_FILES as $file) {
                $fileName = $file['name'];
                $tmpName = $file['tmp_name'];

                if ($file['error'] !== UPLOAD_ERR_OK) {
                    Log::error($file['error']);
                    echo json_encode(null);
                }

                $fp = fopen($tmpName, 'r');
                $content = fread($fp, filesize($tmpName));
                $fileName = preg_replace('/[^A-Za-z0-9 _ .-]/', '', $fileName);
                $filePath = '/media/editor/' . $fileName;

                Storage::put($filePath, $content);

                $url = url(Config::get('cms.storage.media.path')) . '/editor/' . $fileName;

                fclose($fp);

                $result = [
                    'name' => $file['name'],
                    'type' => 'image',
                    'src' => $url,
                    'height' => 350,
                    'width' => 250,
                ];
                $resultArray[] = $result;
            }

            $response = ['data' => $resultArray];
        }

        return Response::json($response);
    }
}
