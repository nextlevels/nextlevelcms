<?php namespace NextLevels\NextLevelCms\Helpers;

/**
 * Class BaseHelper
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>
 */
class BaseHelper
{

    /**
     * Remove html comments
     *
     * @param string $content
     *
     * @return string|string[]|null
     */
    public static function removeHtmlComments(string $content = '')
    {
        return preg_replace('/<!--(.|\s)*?-->/', '', $content);
    }
}
