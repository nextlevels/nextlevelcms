<?php namespace NextLevels\NextLevelCms\Components;

use Cms\Classes\ComponentBase;
use NextLevels\NextLevelCms\Models\Menu;
use NextLevels\NextLevelCms\Models\Page;
use October\Rain\Database\Collection;

/**
 * Class NextCMS
 */
class NextCMS extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name' => 'NextCMS',
            'description' => 'NextCMS'
        ];
    }

    public function onRun(): void
    {
        $this->page['page'] = $this->page;
    }

    public function getPage(): ?string
    {
    }

    /**
     * Get menu pages by menu code
     *
     * @param string $code
     *
     * @return Menu
     */
    public function getMenu(string $code): ?Menu
    {
        return Menu::where('code', $code)->orWhere('name', $code)->first();
    }

    /**
     * Get menu pages by menu code
     *
     * @param string $code
     *
     * @return Collection
     */
    public function getMenuPages(string $code): Collection
    {
        if (($menu = Menu::where('code', $code)->orWhere('name', $code)->first()) !== null) {
            return $menu->pages()->isActive()->orderBy('sort', 'asc')->get();
        }

        return new Collection;
    }

    /**
     * Get page section by url
     *
     * @param string $url
     *
     * @return string
     */
    public function getSectionByURL(string $url)
    {
        if (($page = Page::where('slug', $url)->first()) !== null) {
            return $page->renderHTML();
        }

        return '';
    }
}
