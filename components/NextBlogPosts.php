<?php namespace NextLevels\NextLevelCms\Components;

use Cms\Classes\ComponentBase;
use October\Rain\Database\Collection;
use October\Rain\Database\Model;
use RainLab\Blog\Models\Post as BlogPost;

/**
 * Class NextBlogPosts
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>
 */
class NextBlogPosts extends ComponentBase
{
    /**
     * A collection of posts to display
     *
     * @var Collection
     */
    public $posts;

    /**
     * Parameter to use for the page number
     *
     * @var string
     */
    public $pageParam;

    /**
     * If the post list should be filtered by a category, the model to use
     *
     * @var Model
     */
    public $category;

    /**
     * Message to display when there are no messages
     *
     * @var string
     */
    public $noPostsMessage;

    /**
     * Reference to the page name for linking to posts
     *
     * @var string
     */
    public $postPage;

    /**
     * Reference to the page name for linking to categories
     *
     * @var string
     */
    public $categoryPage;

    /**
     * If the post list should be ordered by another attribute
     *
     * @var string
     */
    public $sortOrder;

    public function componentDetails()
    {
        return [
            'name' => 'rainlab.blog::lang.settings.posts_title',
            'description' => 'rainlab.blog::lang.settings.posts_description'
        ];
    }

    public function defineProperties()
    {
        return [
            'pageNumber' => [
                'title' => 'rainlab.blog::lang.settings.posts_pagination',
                'description' => 'rainlab.blog::lang.settings.posts_pagination_description',
                'type' => 'string',
                'default' => '{{ :page }}',
            ],
            'postsPerPage' => [
                'title' => 'rainlab.blog::lang.settings.posts_per_page',
                'type' => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'rainlab.blog::lang.settings.posts_per_page_validation',
                'default' => '10',
            ],
        ];
    }

    /**
     * List published posts
     *
     */
    public function listPosts()
    {
        //return 3 newest posts
        return BlogPost::all();
    }

    /**
     * List published posts
     *
     * @return Collection
     */
    public function listByKeyword($keyword = ''): Collection
    {
        //return 3 newest posts
        $posts = BlogPost::isPublished()->where('metadata', 'LIKE', '%' . $keyword . '%')->orderBy(
            'published_at',
            'DESC'
        )->take(3)->get();
        if ($posts->count() == 0) {
            $posts = BlogPost::isPublished()->take(3)->get();
        }

        return $posts;
    }
}
